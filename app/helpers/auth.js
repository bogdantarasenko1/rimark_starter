import jsCookie from 'js-cookie'

const parseUser = (user) => user ? JSON.parse(user) : null

export const saveAuth = user => {
  jsCookie.set('jwtToken', user.jwtToken, { expires: 7 })
  jsCookie.set('email', user.email, { expires: 7 })

  jsCookie.set('user', user, { expires: 7 })
  if (!isLocalStorageNameSupported()) {
    return
  }

  window.localStorage.setItem('jwtToken', user.jwtToken)
  window.localStorage.setItem('email', user.email)
  window.localStorage.setItem('user', JSON.stringify(user))
}

export const getUser = () => {
  const user = jsCookie.get('user')
  return parseUser(user)
}

export const getServerUser = (coockie) => {
  const { user } = coockie
  return parseUser(user)
}

export const logout = () => {
  jsCookie.remove('jwtToken')
  jsCookie.remove('email')
  jsCookie.remove('user')
  if (!isLocalStorageNameSupported()) {
    return
  }
  window.localStorage.removeItem('jwtToken')
  window.localStorage.removeItem('email')
  window.localStorage.removeItem('user')
}

export const isLocalStorageNameSupported = () => {
  const testKey = 'test'
  const storage = window.localStorage

  try {
    storage.setItem(testKey, '1')
    storage.removeItem(testKey)
    return true
  } catch (error) {
    return false
  }
}
