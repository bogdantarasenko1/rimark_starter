import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core'

const styles = theme => ({
  img: {
    width: '154px',
    height: '77px'
  }
})

const logoImg = (props) => {
  return <img src="/static/logo.png" className={props.classes.img} />
}

export const Logo = withStyles(styles)(logoImg)

Logo.propTypes = {
  classes: PropTypes.object
}