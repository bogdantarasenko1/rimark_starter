import React, { Fragment } from 'react'
import { Logo } from '../common/logo'

const flexCenter = {
  display: 'flex',
  height: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center'
}

const title = {
  color: '#3c5aff',
  fontWeight: 500,
  fontFamily: 'Montserrat'
}

const SignUp = () => {
  return (
    <Fragment>
      <div style={flexCenter}>
        <Logo />
        <p style={title}>Starter for Rimark</p>
      </div>
    </Fragment>
  )
}

export default SignUp
