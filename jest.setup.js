import * as jest from 'jest'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import $ from 'jquery'
import popperjs from 'popper.js'

global.$ = global.jQuery = $
global.Popper = popperjs

jest.mock('next/link', () => {
  return ({ children }) => {
    return children
  }
})

configure({ adapter: new Adapter() })
