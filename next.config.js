const withPlugins = require('next-compose-plugins')
const css = require('@zeit/next-css')
const nextRuntimeDotenv = require('next-runtime-dotenv')

const withConfig = nextRuntimeDotenv({
  public: ['API_URL', 'API_KEY', 'APOLLO_URL'],
  server: []
})

module.exports = withConfig(
  withPlugins([
    [
      css,
      {
        cssModules: true,
        cssLoaderOptions: {
          importLoaders: 1,
          localIdentName: '[name]_[local]_[hash:base64:5]'
        }
      }
    ]
  ])
)
