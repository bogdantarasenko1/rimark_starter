const express = require('express')
const next = require('next')
const cookieParser = require('cookie-parser')
const routes = require('./serverUtils/routes')
const {
  pagesAccess,
  isAuthenticated,
  MAIN_URI,
  DASHBOARD_URI
} = require('./serverUtils/utils')

const port = process.env.PORT || 3000
const dev = process.env.NODE_ENV !== 'production'

const app = next({ dev })
const handler = routes.getRequestHandler(app)

app
  .prepare()
  .then(() => {
    const server = express()
    server.use(cookieParser())
    server.use(express.static(`${__dirname}/static`))

    server.all('*', pagesAccess)

    server.get('/', (req, res) => {
      res.redirect(DASHBOARD_URI)
    })

    server.get('/home', isAuthenticated, (req, res) => {
      const actualPage = MAIN_URI
      app.render(req, res, actualPage)
    })

    server.use(handler).listen(port, err => {
      if (err) throw err
      console.log('> Ready on http://localhost:3000')
    })
  })
  .catch(ex => {
    console.error(ex.stack)
    process.exit(1)
  })