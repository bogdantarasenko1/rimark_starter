import AuthHook from '../serverUtils/authPageHook'
import Home from '../app/containers/Home/Home.container'

const HomePage = props => (
  <AuthHook
    render={auth => (
      <Home homePage={auth.homePage} saveAuth={auth.saveAuth} {...props} />
    )}
  />
)

export default HomePage
