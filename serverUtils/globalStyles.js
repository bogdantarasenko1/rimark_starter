export default theme => ({
  '@global': {
    'html,body': {
      height: '100%',
      width: '100%'
    },
    body: {
      fontFamily: '"Helvetica Neue", Helvetica, Arial, sans-serif'
    },
    'body.fontLoaded': {
      fontFamily: '"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif'
    },
    '#__next': {
      backgroundColor: 'whitesmoke',
      minHeight: '100%',
      minWidth: '100%',
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'flex-start',
      alignItems: 'stretch',
      alignContent: 'stretch'
    },
    a: {
      color: theme.palette.link.main,
      '&:hover': {
        color: theme.palette.link.hover
      }
    }
  }
})
