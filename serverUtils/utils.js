const MAIN_URI = '/home'
const DASHBOARD_URI = '/dashboard'

const isStaticFilesPath = path => {
  return /^\/(_next|static)/.test(path)
}

const isAuthenticated = (req, res, next) => {
  const { user } = req.cookies
  if (user) {
    return res.redirect(DASHBOARD_URI)
  }
  return next()
}

const pagesAccess = (req, res, next) => {
  if (isStaticFilesPath(req.path)) {
    return next()
  }

  const { user } = req.cookies
  if (user) {
    return next()
  }

  if (
    req.path === MAIN_URI
  ) {
    return next()
  }

  return res.redirect(MAIN_URI)
}

module.exports = {
  isAuthenticated,
  pagesAccess,
  MAIN_URI,
  DASHBOARD_URI
}
