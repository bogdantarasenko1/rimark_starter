import Router from 'next/router'
import PropTypes from 'prop-types'
import { saveAuth, getUser, getServerUser } from '../app/helpers/auth'
import { DASHBOARD_URI as HOMEPAGE } from '../serverUtils/utils'

const authPageHook = (props) => {
  return props.render({ homePage: HOMEPAGE, saveAuth })
}

authPageHook.getInitialProps = async ({ req, res }) => {
  const isServer = !!req
  const user = isServer ? getServerUser(req.cookies) : getUser()

  if (user) {
    if (isServer) {
      res.redirect(HOMEPAGE)
      res.end()
    } else {
      Router.push(HOMEPAGE)
    }
    return {}
  }
}

authPageHook.propTypes = {
  render: PropTypes.object
}

export default authPageHook
